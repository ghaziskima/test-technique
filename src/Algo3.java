
public class Algo3 {

	public static void main(String[] args) {
		int[] A= {1,5,2,-2};
		 
		int res=Solution.solution(A);
		System.out.println("result : "+res);
		 
	}
		
		static class Solution { 
			
			
			
			
			public static int solution(int[] A) {
				int[] s= {-1,1};
				int sum=0;
				
				if(A.length>=0 && A.length<=20000) {
					
					for(int i=0;i<A.length;i++) {
						
						if(A[i]>=-100 && A[i]<=100) {
							 
							if((i+1)%2==0)
								sum+=A[i]*s[1];
							else
								sum+=A[i]*s[0];
						}else
							return 0;
						
						
						
						
					}
					
					
				}else
					return 0;
				
				return sum;
			}
		}

		

}
