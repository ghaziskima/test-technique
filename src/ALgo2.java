 
public class ALgo2 {

	public static void main(String[] args) {
		int[] A= {4,4,5,5,1};
		int[] B= {3,2,4,3,1};
		 
		int[] res=Solution.solution(A,B);
		
		String ResultDis=" ( ";
		for(int i:res)
			ResultDis+= i+" ";
		System.out.println("result : "+ResultDis+")");

		

	}

	
	static class Solution { 
		
		public static int[] solution(int[] A, int[] B) {
			
			int [] tab=new int[A.length];
			
			int[] result= new int[A.length];
			
			if(A.length>=1 && A.length<=50000 && B.length>=1 && B.length<=50000  ) {
				
				for(int i=0;i<A.length;i++) {
					
					if(A[i]>=1 && A[i]<=A.length && B[i]>=1 && B[i]<=30) {
						
						int countA=1;
						int countB=1;
						
						 
						
						int lengthA=A[i]-1;
						int lengthB=B[i]-1;
						while(lengthA>2) {
							
							 
							countA+=lengthA;
							lengthA=lengthA-1;

						}
						
						if(A[i] ==3)
							countA+=2;
						
						if((A[i]%2)==0)
							countA++;
						
						 
						while(lengthB>	2) {
							
							countB+=lengthB;
							lengthB--;
							

						}
						
						if((B[i]%2)==0)
							countB++;
						if(B[i]==3)
							countB+=2;
						
						 
						
						result[i]=countA%(2*countB);
							
							
						
						
						
					}else
						return null;
				}
				
				
			}else
				return null;
			
			
			return result;
			
			
		}
		
	}
}
