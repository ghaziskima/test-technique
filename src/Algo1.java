 
public class Algo1 {

	public static void main(String[] args) {
		int[] A= {3,4,4,6,1,4,4};
		 
		int[] res=Solution.solution(5,A);
		
		String ResultDis=" ( ";
		for(int i:res)
			ResultDis+= i+" ";
		System.out.println("result : "+ResultDis+")");

	}
	
	static class Solution {
		
		
		public static int[] solution(int N, int[] A) {
			
			int[] tab=new int[N];
			
			if(N>=1 && N<=100000 && A.length>=1 && A.length<=100000) {
				 
				
				for(int element:A) {
					
					if(element>=1 && element <= N+1) {
						 
						if(element==N+1) {
							 
							int max=getMaxValue(tab);
							
							for(int i =0; i<tab.length;i++) {
								tab[i]=max;
								 
							}
						}else {
							 
							tab[element-1]++;
							 
						}
						
						 
						
					}
					else
						return null;
					
					
					
				}
				
				
				
				
			}else
				return null;
			
			
			return tab;
		
	}
		
		static int getMaxValue(int[] tab) {
			
			int result=tab[0];
			
			for(int i =1; i<tab.length;i++) {
				if(tab[i]>result)
					result=tab[i];
				
			}
			
			return result;
		}
		
		 
		
		
	}

}
